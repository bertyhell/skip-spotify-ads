chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.reloadSpotifyTab) {
      console.log('background script received reload request');
      chrome.tabs.query({url: "*://open.spotify.com/*"}, function(tabs) {

        let tabId = tabs[0].id;
        console.log('active tab id is: ' + tabId);
        chrome.tabs.reload(tabId, {bypassCache: true}, () => {
          sendResponse({reloadDone: true});
        });

      });
      return true;
    } else {
      sendResponse({message: 'No spotify tabs found'});
    }
});
