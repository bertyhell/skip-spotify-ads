/// <reference path="../../node_modules/@types/chrome/index.d.ts" />

console.log('Reload spotify is listening');

if (localStorage.getItem('spotifyPlayAfterReload') === 'true') {
  localStorage.removeItem('spotifyPlayAfterReload');
  console.log('page has been reloaded: clicking play button');
  // Play after reload
  window.addEventListener('load', () => {
    const playButton = document.querySelector('.player-controls .control-button[title="Play"]') as HTMLElement;
    if (playButton) {
      setTimeout(() => {
        simulateClick(playButton);
      }, 1000);
    } else {
      console.error('play button was not found after page reload: .player-controls .control-button[title="Play"]');
    }
  });
}

window.addEventListener('load', () => {
  setInterval(() => {
    let nowPlayingAnchor = document.querySelector('.now-playing-bar__left a');

    if (!nowPlayingAnchor)
      return;

    let linkElem: HTMLAnchorElement = nowPlayingAnchor as HTMLAnchorElement;
    if (linkElem) {
      let url = linkElem.href;

      if (url.indexOf('/user/spotify/') === -1) {
        // Commercial
        console.log('detected commercial: ' + url);
        localStorage.setItem('spotifyPlayAfterReload', 'true');
        sendMessageToBackground({
          reloadSpotifyTab: true
        }, () => {});
      }
    }

  }, 100); // Wait for spotify to init the "now playing" html
});

function sendMessageToBackground(obj: any, callback: Function) {
  obj.target = 'background';
  chrome.runtime.sendMessage(obj, (response) => {
    callback(response);
  });
}

/**
 * Simulate a click event.
 * @public
 * @param {Element} elem  the element to simulate a click on
 */
function simulateClick(elem: HTMLElement) {
  // Create our event (with options)
  const evt = new MouseEvent('click', {
    bubbles: true,
    cancelable: true,
    view: window
  });
  // If cancelled, don't dispatch our event
  !elem.dispatchEvent(evt);
}
