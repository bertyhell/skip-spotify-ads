console.log('Reload spotify is listening');
if (localStorage.getItem('spotifyPlayAfterReload') === 'true') {
    localStorage.removeItem('spotifyPlayAfterReload');
    console.log('page has been reloaded: clicking play button');
    window.addEventListener('load', function () {
        var playButton = document.querySelector('.player-controls .control-button[title="Play"]');
        if (playButton) {
            setTimeout(function () {
                simulateClick(playButton);
            }, 1000);
        }
        else {
            console.error('play button was not found after page reload: .player-controls .control-button[title="Play"]');
        }
    });
}
window.addEventListener('load', function () {
    setInterval(function () {
        var nowPlayingAnchor = document.querySelector('.now-playing-bar__left a');
        if (!nowPlayingAnchor)
            return;
        var linkElem = nowPlayingAnchor;
        if (linkElem) {
            var url = linkElem.href;
            if (url.indexOf('/user/spotify/') === -1) {
                console.log('detected commercial: ' + url);
                localStorage.setItem('spotifyPlayAfterReload', 'true');
                sendMessageToBackground({
                    reloadSpotifyTab: true
                }, function () { });
            }
        }
    }, 100);
});
function sendMessageToBackground(obj, callback) {
    obj.target = 'background';
    chrome.runtime.sendMessage(obj, function (response) {
        callback(response);
    });
}
function simulateClick(elem) {
    var evt = new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window
    });
    !elem.dispatchEvent(evt);
}
//# sourceMappingURL=content.js.map