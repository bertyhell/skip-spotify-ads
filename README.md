# Skip Spotify Ads
Chrome Browser extensions that reloads the page when it detects an ad and then resumes playback.

![Overview of steps: detect, reload, resume](assets/screenshot-1.png)
